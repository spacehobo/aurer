package main

import (
	"os"
	_ "os/user"
	"path"
	_ "strings"
	"testing"
)

func TestPresenceMain(t *testing.T) {
	//t.Error("Testing works fine")
}

func TestFunctionIsDir(t *testing.T) {
	if !IsDir("/tmp") {
		t.Error("Incorrect detection of a directory presence")
	}
}

func TestFunctionIsDirOnFile(t *testing.T) {
	pwd, _ := os.Getwd()
	file := path.Join(pwd, "aurer_test.go")
	if IsDir(file) {
		t.Errorf("A file %s considered as a directory", file)
	}
}

func TestFunctionIsDirOnNoneExistent(t *testing.T) {
	notexist := "/noneexistent"
	if IsDir(notexist) {
		t.Errorf("None existent entity is considered as a directory: %s", notexist)
	}
}

func TestGetHomeDir(t *testing.T) {
	homedir := GetHomeDir()
	if homedir != os.Getenv("HOME") {
		t.Error("GetHomeDir() returns wrong homedir", homedir, os.Getenv("HOME"))
	}
}

func TestFuncExpandHomeDir(t *testing.T) {
	// homedir := GetHomeDir()
	homepath := "~/.config"
	abspath := "/etc"

	if ExpandHomeDir(homepath) != os.ExpandEnv("${HOME}/.config") {
		t.Error("Incorrect expanding home directory", ExpandHomeDir(homepath), os.ExpandEnv("${HOME}/.config"))
	}
	if ExpandHomeDir(abspath) != abspath {
		t.Errorf("A none-home directory path got broken: %s instead %s", ExpandHomeDir(abspath),  abspath)
	}
}
