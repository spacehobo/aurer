package main

import (
	"strings"
	"testing"
)

var LOREM_IPSUM string = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`

func TestWrappingShortString(t *testing.T) {
	text := "short string"

	wrapped, err := FormatText(text, 50, 0)
	if err != nil {
		t.Error("Unexpected error")
	}
	if text != wrapped {
		t.Error("Strings shorter than wrap size should be changed")
	}
}

func TestWrappingLongString(t *testing.T) {
	text := LOREM_IPSUM
	wrapsize := 40

	wrapped, err := FormatText(text, wrapsize, 0)
	if err != nil {
		t.Error("unexpected error", err)
	}
	for i, s := range strings.Split(wrapped, "\n") {
		if len(s) > wrapsize {
			t.Errorf("Raw %d is longer than %d", i, wrapsize)
		}
	}
}

func TestIndentationOfWrappedText(t *testing.T) {
	text := LOREM_IPSUM
	wrapsize := int(60)
	indent := 4

	wrapped, err := FormatText(text, wrapsize, indent)
	if err != nil {
		t.Error("unexpected error", err)
	}

	spaces := strings.Repeat(" ", indent)
	for i, s := range strings.Split(wrapped, "\n") {
		if !strings.HasPrefix(s, spaces) || s[indent] == byte(" "[0]) {
			t.Errorf("Raw #%d is indented incorrectly: \n\t%s", i, s)
		}
	}
}

func TestUnwrappingTextOnWordLongerWrapSize(t *testing.T) {
	text := LOREM_IPSUM
	wrapsize := int(12)

	wrapped, err := FormatText(text, wrapsize, 0)
	if err == nil {
		t.Error("should return error on too long words")
	}
	if text != wrapped {
		t.Error("should return unchanged text")
	}
}
