package main

import (
	"flag"
	"fmt"
	"os"
)

type Action int64

const (
	_ Action = iota
	ActionSearch
	ActionInfo
	ActionDwnld
	ActionSearchOrInfo
)

var USAGE string = `Usage: aurer [-d|-i|-s] [<pkg_pattern>]
  <pkg_pattern>    search word or package name
flags:
`

type CmdLine struct {
	Action Action
	Args   []string
}

func (c *CmdLine) Init() {
	var flSearch, flInfo, flDwnld bool

	flag.Usage = func() {
		fmt.Print(USAGE)
		flag.PrintDefaults()
	}

	flag.BoolVar(&flSearch, "s", false, "search packages by the pattern in AUR repository")
	flag.BoolVar(&flInfo, "i", false, "get details of the AUR package")
	flag.BoolVar(&flDwnld, "d", false, "download and extract the AUR package in AUR sources directory")
	flag.Parse()
	c.Args = flag.Args()
	switch {
	case len(flag.Args()) == 0:
		flag.Usage()
		os.Exit(0)
	case flSearch && !flInfo && !flDwnld:
		c.Action = ActionSearch
	case !flSearch && flInfo && !flDwnld:
		c.Action = ActionInfo
	case !flSearch && !flInfo && flDwnld:
		c.Action = ActionDwnld
	case !flSearch && !flInfo && !flDwnld:
		c.Action = ActionSearchOrInfo
	default:
		fmt.Println("Executed Default Case")
		os.Exit(0)
	}
}
