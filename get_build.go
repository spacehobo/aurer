package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
	_ "os/exec"
	"path/filepath"
)

var (
	SOURCES_DIR string = "./sources"
	TEMP_DIR    string = "/tmp"
)

func PackageDir(fname string) (string, error) {
	var buf bytes.Buffer

	cmd := []string{"tar", "tzf", fname}
	if err := ExecuteWithOutput(cmd, &buf); err != nil {
		return "", err
	}
	dirname, err := buf.ReadString([]byte("\n")[0])
	if err != nil {
		return "", err
	}

	// log.Print("extracted dirname: ", dirname)
	return dirname, nil
}

func DownloadAndExtract(url string, path string) (srcdir string, err error) {
	fd, err := os.CreateTemp(TEMP_DIR, "aurer")
	if err != nil {
		log.Print("error: cannot create temporary file at ", TEMP_DIR)
		return srcdir, err
	}
	fname := fd.Name()

	cmd := []string{"wget", "-q", "-O", "-", url}
	if err = ExecuteWithOutput(cmd, fd); err != nil {
		log.Print("error: downloading fails")
		return srcdir, err
	}
	log.Print("Temp file: ", fname)

	cmd = []string{"tar", "xvzf", fname, "-C", path}
	if err = ExecuteWithOutput(cmd, os.Stdout); err != nil {
		log.Print("error: package extracting fails")
		return srcdir, err
	}

	pkgdir, err := PackageDir(fname)
	if err != nil {
		log.Print("error: cannot get the package base directory")
		return srcdir, err
	}

	fd.Close()
	err = os.Remove(fname)
	if err != nil {
		log.Print("error: cannot remove temp file: ", fname)
	}

	srcdir, err = filepath.Abs(filepath.Join(path, pkgdir))
	if err != nil {
		log.Print("error: cannot get an absolute path of the extracted package")
		return srcdir, err
	}

	fmt.Print("Extracted source directory: ", srcdir)
	return
}
