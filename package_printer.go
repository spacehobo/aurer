package main

import (
	"bytes"
	"fmt"
	aur "northbear/aurer/aurweb_rpc"
	"strings"
	"time"
)

type PackagePrinter struct {
	aur.Package
}

func NewPkgPrinter(p aur.Package) *PackagePrinter {
	var pp *PackagePrinter = &PackagePrinter{p}
	return pp
}

func (pp *PackagePrinter) ShortDescr() string {
	var buffer bytes.Buffer

	formatted, _ := FormatText(pp.Description, 75, 4)
	fmt.Fprintf(&buffer, "%s %s\n", pp.Name, pp.Version)
	fmt.Fprintf(&buffer, "%s", formatted)

	return buffer.String()
}

func csv(l []string) string {
	return strings.Join(l, ", ")
}

func (pp *PackagePrinter) Detailed() string {
	var buffer bytes.Buffer
	formatted, _ := FormatText(pp.Description, 50, 17)
	fmt.Fprintf(&buffer, "Name           : %s\n", pp.Name)
	fmt.Fprintf(&buffer, "Version        : %s\n", pp.Version)
	fmt.Fprintf(&buffer, "Description    : %s\n", formatted[17:])
	fmt.Fprintf(&buffer, "URL            : %s\n", pp.URL)
	fmt.Fprintf(&buffer, "NumVotes       : %d\n", pp.NumVotes)
	fmt.Fprintf(&buffer, "Popularity     : %f\n", pp.Popularity)
	fmt.Fprintf(&buffer, "OutOfDate      : %d\n", pp.OutOfDate)
	fmt.Fprintf(&buffer, "Maintainer     : %s\n", pp.Maintainer)
	fmt.Fprintf(&buffer, "FirstSubmitted : %s\n", time.Unix(pp.FirstSubmitted, 0).UTC())
	fmt.Fprintf(&buffer, "LastModified   : %s\n", time.Unix(pp.LastModified, 0).UTC())
	if len(pp.Depends) != 0 {
		fmt.Fprintf(&buffer, "Depends        : %s\n", csv(pp.Depends))
	}
	if len(pp.MakeDepends) != 0 {
		fmt.Fprintf(&buffer, "MakeDepends    : %s\n", csv(pp.MakeDepends))
	}
	if len(pp.OptDepends) != 0 {
		fmt.Fprintf(&buffer, "OptDepends     : %s\n", csv(pp.OptDepends))
	}
	if len(pp.CheckDepends) != 0 {
		fmt.Fprintf(&buffer, "CheckDepends   : %s\n", csv(pp.CheckDepends))
	}
	if len(pp.Conflicts) != 0 {
		fmt.Fprintf(&buffer, "Conflicts      : %s\n", csv(pp.Conflicts))
	}
	if len(pp.Provides) != 0 {
		fmt.Fprintf(&buffer, "Provides       : %s\n", csv(pp.Provides))
	}
	if len(pp.Provides) != 0 {
		fmt.Fprintf(&buffer, "Replaces       : %s\n", csv(pp.Replaces))
	}
	if len(pp.Replaces) != 0 {
		fmt.Fprintf(&buffer, "License        : %s\n", csv(pp.License))
	}
	return buffer.String()
}
