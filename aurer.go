package main

import (
	"confini"
	"fmt"
	"log"
	aur "northbear/aurer/aurweb_rpc"
	"os"
	"os/user"
	"path"
	"path/filepath"
	"strings"

	_ "github.com/urfave/cli/v2"
)

const AURER_CONF_FILE string = "aurer/config.ini"

func IsDir(pn string) bool {
	fi, err := os.Stat(pn)
	if os.IsNotExist(err) || !fi.IsDir() {
		return false
	}
	return true
}

func GetHomeDir() string {
	usr, _ := user.Current()
	return usr.HomeDir
}

func ExpandHomeDir(pth string) string {
	if strings.HasPrefix(pth, "~/") {
		return path.Join(GetHomeDir(), pth[2:])
	}
	return pth
}

type Context struct {
	cmdline CmdLine
	pkg     Pkg
	homedir string
	aururl  string
	aurdir  string
}

type Pkg struct {
	name string
	aur  aur.Package
}

type Task func(*Context) error

var Execute map[Action]Task = map[Action]Task{
	ActionSearchOrInfo: DefaultTask,
	ActionDwnld:        DownloadAur,
	ActionInfo:         GetAurInfo,
	ActionSearch:       MakeAurSearch,
}

func NewContext() *Context {
	ctx := &Context{}
	ctx.cmdline.Init()
	return ctx
}

func (c *Context) LoadConfig() error {
	c.homedir = os.Getenv("HOME")
	c.aururl = "https://aur.archlinux.org"
	c.aurdir = path.Join(c.homedir, "sources/")
	if !IsDir(c.aurdir) {
		return fmt.Errorf("aurdir does not exist: %s", c.aurdir)
	}
	return nil
}

func (c *Context) Execute() error {
	err := Execute[c.cmdline.Action](c)
	return err
}

func GetAurInfo(ctx *Context) error {
	args := ctx.cmdline.Args
	resp, err := aur.GetInfo(args)
	if err != nil {
		return err
	}

	for _, p := range resp.Results {
		fmt.Println(NewPkgPrinter(p).Detailed())
	}
	return nil
}

func DownloadAur(ctx *Context) error {
	resp, err := aur.GetInfo(ctx.cmdline.Args)
	if err != nil {
		return err
	}

	if !(resp.ResultCount > 0) {
		pkgname := ctx.cmdline.Args[0]
		return fmt.Errorf("No AUR package with this name: %s", pkgname)
	}
	ctx.pkg.aur = resp.Results[0]
	fmt.Printf("Download and unpack: %s %s\n", ctx.pkg.aur.Name, ctx.pkg.aur.Version)
	srcdir, err := DownloadAndExtract(ctx.aururl+ctx.pkg.aur.URLPath, ctx.aurdir)
	if err != nil {
		return err
	}
	ctx.aurdir = srcdir
	return nil
}

func MakeAurSearch(ctx *Context) error {
	pattern := ctx.cmdline.Args[0]
	resp, err := aur.GetSearch(pattern, "name-desc")
	if err != nil {
		return err
	}

	for _, p := range resp.Results {
		fmt.Println(NewPkgPrinter(p).ShortDescr())
	}
	return nil
}

func Usage(ctx *Context) error {
	fmt.Println("Usage: aurer [<option>] [pkgname|pattern]")
	return nil
}

func DefaultTask(ctx *Context) error {
	pattern := ctx.cmdline.Args[0]
	resp, err := aur.GetSearch(pattern, "name-desc")
	if err != nil {
		return err
	}

	for _, p := range resp.Results {
		if pattern == p.Name {
			fmt.Println(NewPkgPrinter(p).Detailed())
			return nil
		}
	}

	for _, p := range resp.Results {
		fmt.Println(NewPkgPrinter(p).ShortDescr())
		// fmt.Printf("%s:\n\t%s\n\n", p.Name, p.Description)
	}
	return nil
}

type AurerConfig struct {
	// ConfigDir  string
	// ConfigFile string
	LogFile    string
	PackageDir string
}

func (ac *AurerConfig) Init(cfg AurerConfig) {
	*ac = cfg
}

func (ac AurerConfig) GetLogFile() string {
	if strings.HasPrefix(ac.LogFile, "/") {
		return ac.LogFile
	}
	return filepath.Join(ExpandHomeDir(ac.ConfigDir()), ac.LogFile)
}

// func (ac AurerConfig) GetConfigFile() string {
// 	cfname := path.Base(AURER_CONF_FILE)
// 	return filepath.Join(ExpandHomeDir(ac.ConfigDir()), ac.ConfigFile)
// }

func (ac AurerConfig) ConfigDir() string {
	cfname, _ := confini.Discover(AURER_CONF_FILE)
	return path.Dir(cfname)
}

var DEFAULT_CONFIG = AurerConfig{
	// ConfigDir:  "~/.config/aurer",
	// ConfigFile: "aurer.ini",
	LogFile:    "aurer.log",
	PackageDir: "~/sources",
}
var config AurerConfig

func main() {
	logger := log.Default()
	logger.SetPrefix("Aurer: ")
	logger.SetFlags(log.Flags() | log.Lmsgprefix)

	cfname, err := confini.Discover(AURER_CONF_FILE)
	if err == confini.ErrConfigNotFound {
		logger.Printf("A config file not found. Creating a default config file: %s", cfname)
		config = DEFAULT_CONFIG
		err := confini.CreateConfigFile(cfname, config)
		if err != nil {
			logger.Fatal("Error on creating default config: ", err)
		}
	}

	// config.Init(DEFAULT_CONFIG)
	err = confini.LoadConfig(cfname, &config)

	lf := config.GetLogFile()
	lfd, err := os.OpenFile(lf, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		log.Fatal("log file isn't created:", lf)
	}
	defer lfd.Close()
	logger.SetOutput(lfd)

	ctx := NewContext()

	err = ctx.LoadConfig()
	if err != nil {
		log.Fatal(err)
	}

	// logconf, err := NewLogConfig("")
	// if err != nil {
	// 	log.Fatal("Cannot initialize log: ", err)
	// }
	// defer logconf.Close()

	err = ctx.Execute()
	if err != nil {
		fmt.Println(err)
		log.Fatal(err)
	}
}
