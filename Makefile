SOURCE_FILES := aurer.go get_build.go command_execution.go formattext.go logconfig.go package_printer.go cmdline.go
BINARY  := aurer
# SOURCES=(aurer.go args.go)

.PHONY: all test build clean

all: test build

## aurer:   aurer.go get_build.go command_execution.go formattext.go logconfig.go package_printer.go
aurer: $(SOURCE_FILES)
	go build $^

build: aurer

install: $(SOURCE_FILES)
	go install $^

test:
	go test

clean:
	[[ -x aurer ]] && rm aurer
