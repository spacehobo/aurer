package main

import (
	"bytes"
	"fmt"
	"strings"
)

func FormatText(text string, wrapsize int, indentsize int) (string, error) {
	var b bytes.Buffer
	indent := strings.Repeat(" ", indentsize)
	cur := 0
	for len(text[cur:]) > wrapsize {
		next := strings.LastIndex(text[cur:cur+wrapsize], " ")
		if next == -1 {
			return text, fmt.Errorf("the text contains words longer than wrap size")
		}
		b.WriteString(indent + text[cur:cur+next] + "\n")
		cur += next + 1
	}
	b.WriteString(indent + text[cur:])
	return b.String(), nil
}
