package main

import (
	"bytes"
	"log"
	"os"
	"strings"
	"testing"
)

func TestNewExecutorWithLog(t *testing.T) {
	e := NewExecutor(log.Default())
	if e.Log != log.Default() {
		t.Error("logger is not correctly initialized")
	}
}

func TestExecutorExec(t *testing.T) {
	fname := "/tmp/test_executor"

	e := NewExecutor(nil)
	err := e.Exec("touch", fname)
	if err != nil {
		t.Error("unexpected error: ", err)
	}

	err = os.Remove(fname)
	if err != nil {
		t.Error("command is not executed: ", err)
	}
}

func TestExecutorExecTo(t *testing.T) {
	var b bytes.Buffer

	e := NewExecutor(nil)
	err := e.ExecTo(&b, "uname", "-a")
	if err != nil {
		t.Error("unexpected error: ", err)
	}
	if !strings.Contains(b.String(), "Linux") {
		t.Error("wrong output: ", b.String())
	}
}

func TestExecWithOutput(t *testing.T) {
	var buf bytes.Buffer

	err := ExecuteWithOutput([]string{"uname", "-a"}, &buf)
	if err != nil {
		t.Error("unexpected error: ", err)
	}
	if !strings.Contains(buf.String(), "Linux") {
		t.Error("wrong output: ", buf.String())
	}
}
