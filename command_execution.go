package main

import (
	_ "bytes"
	_ "fmt"
	"io"
	"log"
	"os"
	"os/exec"
)

type Executor struct {
	Log *log.Logger
}

func NewExecutor(l *log.Logger) *Executor {
	return &Executor{Log: l}
}

func (e *Executor) Exec(cmd string, args ...string) error {
	action := exec.Command(cmd, args...)
	if e.Log != nil {
		action.Stdout = e.Log.Writer()
		action.Stderr = e.Log.Writer()
	}

	err := action.Run()
	if err != nil {
		return err
	}
	return nil
}

func (e *Executor) ExecTo(w io.Writer, cmd string, args ...string) error {
	action := exec.Command(cmd, args...)
	if e.Log != nil {
		action.Stdout = io.MultiWriter(w, e.Log.Writer())
		action.Stderr = e.Log.Writer()
	} else {
		action.Stdout = w
	}

	err := action.Run()
	if err != nil {
		return err
	}
	return nil
}

func (e *Executor) ExecToConsole(cmd string, args ...string) error {
	action := exec.Command(cmd, args...)
	if e.Log != nil {
		action.Stdout = io.MultiWriter(os.Stdout, e.Log.Writer())
		action.Stderr = io.MultiWriter(os.Stdout, e.Log.Writer())
	} else {
		action.Stdout = os.Stdout
		action.Stderr = os.Stdout
	}

	err := action.Run()
	if err != nil {
		return err
	}
	return nil
}

func ExecuteWithOutput(args []string, o io.Writer) error {
	cmd := exec.Command(args[0], args[1:]...)
	cmd.Stderr, cmd.Stdout = log.Writer(), o
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}
