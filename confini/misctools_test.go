package confini

import (
	"bytes"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestIsFile(t *testing.T) {
	exist_file := "confini_test.go"
	abspath, err := filepath.Abs(exist_file)
	if err != nil {
		t.Error(err)
	}
	absdir := filepath.Dir(abspath)

	if IsFile("/tmp/blablastuff") {
		t.Error("Incorrect existance detection for none-existant file")
	}
	if !IsFile(abspath) {
		t.Errorf("Incorrect existance detection for existant file: %s", abspath)
	}
	if IsFile(absdir) {
		t.Error("Incorrect treating a directory as a file")
	}
}

func TestIsDir(t *testing.T) {
	exist_file := "confini_test.go"
	abspath, err := filepath.Abs(exist_file)
	if err != nil {
		t.Error(err)
	}
	absdir := filepath.Dir(abspath)

	if IsDir("/tmp/blablastuff") {
		t.Error("Incorrect existance detection for a none-existant directory")
	}
	if IsDir(abspath) {
		t.Error("Incorrect treating a file as a directory")
	}
	if !IsDir(absdir) {
		t.Errorf("Incorrect existance detection for the existant directory: %s", abspath)
	}
}

func TestStringsFromFile(t *testing.T) {
	var buf bytes.Buffer
	filename := "testing/config.ini"

	fd, err := os.Open(filename)
	if err != nil {
		t.Error("file not exists:", filename)
	}
	defer fd.Close()

	lines, err := StringsFromFile(filename)
	if err != nil {
		t.Error("Unexpected error StringsFromFile")
	}

	_, err = buf.ReadFrom(fd)
	if err != nil {
		t.Error("unexected error of reading file:", filename)
	}

	for _, s := range lines {
		if !strings.Contains(buf.String(), s) {
			t.Error("lines loads incorrectly. the string is wrong:", s)
		}
	}
}
