package confini

import (
	"testing"
)

func TestBoolConvInput(t *testing.T) {
	trues := []string{"yes", "true", "1"}
	falses := []string{"no", "false", "0"}

	boolConv := BoolConv{}

	for _, value := range trues {
		if v, _ := boolConv.Input(value); v != true {
			t.Errorf("bool true value conversion incorrect for value: %s", value)
		}
	}
	for _, value := range falses {
		if v, _ := boolConv.Input(value); v != false {
			t.Errorf("bool false value conversion incorrect for value: %s", value)
		}
	}
	if _, err := boolConv.Input("incorrect value"); err == nil {
		t.Error("incorrect value for bool doesn't produce error")
	}
}

func TestBoolConvOutput(t *testing.T) {
	bc := BoolConv{}

	if yes := bc.Output(true); yes != "yes" {
		t.Errorf("incorrect bool value true conversion: %s", yes)
	}

	if no := bc.Output(false); no != "no" {
		t.Errorf("incorrect bool value false conversion: %s", no)
	}
}

func TestInt64ConvConsistency(t *testing.T) {
	var num int64 = 10
	ic := Int64Conv{}

	str := ic.Output(num)
	if str != "10" {
		t.Errorf("Incorrect convertion number %d to string: %s", num, str)
	}
	if v, _ := ic.Input(str); v != num {
		t.Errorf("incorrect conversion string \"%s\" to int: %d", str, v)
	}
}

func TestConvertCamelCaseToLowerUnderscore(t *testing.T) {
	camel := "CamelCaseString"
	result := ConvStructToFile(camel)
	if result != "camel_case_string" {
		t.Errorf("incorrect conversion CamelCase to LowerUnderscore: %s", result)
	}
}
