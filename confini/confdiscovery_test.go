package confini

import (
	"os"
	"strings"
	"testing"
)

func TestConfigDiscoveryWithHome(t *testing.T) {
	homepath := os.ExpandEnv("${HOME}/.config")
	fc, _ := Discover("testing/config.ini")
	if !strings.HasPrefix(fc, homepath) {
		t.Error("wrong path with XDG_CONFIG_HOME undefined:", fc)
	}
}

func TestConfigDiscoveryWithXdgPath(t *testing.T) {
	xdgpath := "/tmp"
	os.Setenv("XDG_CONFIG_HOME", xdgpath)
	fc, _ := Discover("testing/config.ini")
	if !strings.HasPrefix(fc, xdgpath) {
		t.Error("wrong path with XDG_CONFIG_HOME defined:", fc)
	}
}

func TestConfigDiscoveryConfigFound(t *testing.T) {
	xdgpath := "."
	os.Setenv("XDG_CONFIG_HOME", xdgpath)
	fc, err := Discover("testing/config.ini")
	if err != nil {
		t.Error("error is not issued if a config is missin:", fc)
	}
}

func TestConfigDiscoveryConfigNotFound(t *testing.T) {
	xdgpath := "noneexistentdir"
	os.Setenv("XDG_CONFIG_HOME", xdgpath)
	fc, err := Discover("testing/config.ini")
	if err == nil {
		t.Error("error is issued if a config exists:", fc)
	}
}
