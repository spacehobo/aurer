package confini

import (
	"bytes"
	"os"
	"strings"
	"testing"
)

func TestSaveToFile(t *testing.T) {
	conffile := "testconfig.ini"
	os.Remove(conffile)
	defer os.Remove(conffile)

	iniconf := NewConfigGenerator(conffile, DefaultConfig)

	err := iniconf.Serialize()
	if err != nil {
		t.Error("Serialization error:", err)
	}

	err = iniconf.SaveToFile()
	if err != nil {
		t.Errorf("Saving to the file %s fails with error: ", err)
	}

	if !IsFile(conffile) {
		t.Error("The file is not created: ", conffile)
	}

	tf, err := os.Open(conffile)
	if err != nil {
		t.Error("cannot open tested file")
	}
	testbuf := bytes.Buffer{}
	_, err = testbuf.ReadFrom(tf)
	if err != nil {
		t.Error("a buffering the file content fails:", err)
	}

	if !strings.Contains(testbuf.String(), iniconf.serialized.String()) {
		println("loaded buffer content: ", testbuf.String())
		println("serialized content: ", iniconf.serialized.String())
		t.Error("the string value serialized incorrectly")
	}
}

func TestLoadFromFile(t *testing.T) {
	conffile := "testing/config.ini"

	iniconf := NewConfigGenerator(conffile, DefaultConfig)

	err := iniconf.LoadFromFile()
	if err != nil {
		t.Error("error of loading content from config file", err)
	}

	if !strings.Contains(iniconf.serialized.String(), DefaultConfig.DestDir) {
		t.Error("the string value serialized incorrectly")
	}

	if !strings.Contains(iniconf.serialized.String(), "clean_dest_before_download") {
		t.Error("the string value serialized incorrectly")
	}
}

func TestCreateConfigFile(t *testing.T) {
	conffile := "create_config_testconfig.ini"
	os.Remove(conffile)
	defer os.Remove(conffile)

	if !IsFile(conffile) {
		err := CreateConfigFile(conffile, DefaultConfig)
		if err != nil {
			t.Error("unexpected error:", err)
		}
	}

	if !IsFile(conffile) {
		t.Error("CreateConfigFile does not create a config file")
	}
}
