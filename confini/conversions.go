package confini

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
	"unicode"
)

var (
	boolTrues  []string = []string{"yes", "true", "1"}
	boolFalses []string = []string{"no", "false", "0"}
)

type BoolConv struct{}

func (bc BoolConv) Input(v string) (bool, error) {
	switch strings.TrimSpace(v) {
	case "yes", "true", "1":
		return true, nil
	case "no", "false", "0":
		return false, nil
	default:
		return false, fmt.Errorf("incorrect value for type bool: %s", v)
	}
}

func (bc BoolConv) Output(v bool) string {
	if v {
		return boolTrues[0]
	}
	return boolFalses[0]
}

type Int64Conv struct{}

func (ic Int64Conv) Input(v string) (int64, error) {
	return strconv.ParseInt(v, 10, 0)
	// return value, err
}

func (ic Int64Conv) Output(v int64) string {
	return strconv.FormatInt(v, 10)
}

func ConvStructToFile(cc string) string {
	var converted bytes.Buffer
	sep := ""
	for _, r := range cc {
		if unicode.IsUpper(r) {
			item := sep + string(unicode.ToLower(r))
			converted.WriteString(item)
		} else {
			converted.WriteRune(r)
		}
		sep = "_"
	}

	return converted.String()
}
