package confini

import (
	"bytes"
	"strings"
	"testing"
)

type AurConfig struct {
	Url               string
	ConnectionTimeout int64
}

type Configuration struct {
	DestDir                 string
	CleanDestBeforeDownload bool
	Aur                     AurConfig
	NotificationMessage     string
}

type WrongConfiguration struct {
	DestDir                 string
	UnsupportedField        uint
	CleanDestBeforeDownload bool
	ConnectionTimeout       int64
	Aur                     AurConfig
	NotificationMessage     string
}

var DefaultConfig Configuration = Configuration{
	DestDir:                 "~/sources",
	CleanDestBeforeDownload: false,
	Aur: AurConfig{
		Url:               "http://some.shit.domain/",
		ConnectionTimeout: 3,
	},
}

func TestConvToIni(t *testing.T) {
	var inibuf bytes.Buffer

	err := ConvToIni(&inibuf, DefaultConfig)
	if err != nil {
		t.Error("unexpected fail")
	}

	if !strings.Contains(inibuf.String(), DefaultConfig.DestDir) {
		t.Error("the string value serialized incorrectly")
	}

	if !strings.Contains(inibuf.String(), "clean_dest_before_download") {
		t.Error("the string value serialized incorrectly")
	}
}

func TestMakeFieldMap(t *testing.T) {
	fm, err := MakeFieldMap(DefaultConfig)
	if err != nil {
		t.Error("Unexpected error:", err)
	}
	if len(fm) != 6 {
		t.Error("FieldMap built up incorrectly")
	}
}

func TestMakeFieldMapWrongConfig(t *testing.T) {
	wrongconf := WrongConfiguration{}
	_, err := MakeFieldMap(wrongconf)
	if err == nil {
		t.Error("do not return error for config with unsupported field type")
	}
}

func TestConfigParserParseConfig(t *testing.T) {
	config := Configuration{}
	filename := "testing/config.ini"
	cp := NewConfigParser(&config)

	err := cp.Parse(filename)
	if err != nil {
		t.Error("Unexpected error ParseConfigFile:", err)
	}
}

func TestSetValueByReflect(t *testing.T) {
	config := Configuration{}

	err := SetStructValue(&config, "DestDir", "new_dest_dir")
	if err != nil {
		t.Error("unexpected error")
	}
	if config.DestDir != "new_dest_dir" {
		t.Error("SetStructValue doesn't set the value correctly")
	}
}

func TestLoadConfig(t *testing.T) {
	config := Configuration{}
	filename := "testing/config.ini"

	err := LoadConfig(filename, &config)
	if err != nil {
		t.Error("Unexpected error:", err)
	}
}
