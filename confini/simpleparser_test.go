package confini

import (
	"bytes"
	"os"
	"testing"
)

type TestConfig struct {
	ConfigDir     string `ini:"config_directory"`
	PackageDir    string `ini:"package_directory"`
	TimeOut       int64  `ini:"timeout"`
	EnableLogging bool   `ini:"enable_logging"`
}

func TestNewSimpleIniParser(t *testing.T) {
	var tc TestConfig

	inifile := "testing/simple.ini"
	inif, err := os.Open(inifile)
	if err != nil {
		t.Error("Unexpected: ", err)
	}
	defer inif.Close()

	parser := NewSimpleIniParser(inif)
	err = parser.Parse(&tc)
	if err != nil {
		t.Error("Unexpected error: ", err)
	}

	if tc.TimeOut != 60 {
		t.Errorf("Wrong parsing int64 value: %d, expected %d", tc.TimeOut, 60)
	}
	if tc.PackageDir != "~/sources" {
		t.Errorf("Wrong parsing string value: %s, expected %s", tc.PackageDir, "~/sources")
	}
	if tc.EnableLogging != true {
		t.Errorf("Wrong parsing Bool value: %t, expected %t", tc.EnableLogging, true)
	}
}

func TestParseIni(t *testing.T) {
	var buff bytes.Buffer
	var inikv IniKV
	ininame := "testing/simple.ini"
	df, err := os.Open(ininame)
	if err != nil {
		t.Error("Unexpected error", err)
	}
	defer df.Close()
	buff.ReadFrom(df)

	inikv = ParseIni(buff)
	if inikv["timeout"] != "60" {
		t.Errorf("Incorrect parsing. Get %s, expected %s", inikv["timeout"], "60")
	}
	if inikv["package_directory"] != "~/sources" {
		t.Errorf("Incorrect parsing. Get %s, expected %s", inikv["package_directory"], "~/sources")
	}
	if inikv["enable_logging"] != "yes" {
		t.Errorf("Incorrect parsing. Get %s, expected %s", inikv["enable_logging"], "yes")
	}
}
