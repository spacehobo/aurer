package confini

import (
	"fmt"
	"os"
	"path"
)

var ErrConfigNotFound error = fmt.Errorf("Config file not found")

func Discover(pth string) (string, error) {
	configdir := os.Getenv("XDG_CONFIG_HOME")
	if configdir == "" {
		configdir = os.ExpandEnv("${HOME}/.config")
	}

	if fp := path.Join(configdir, pth); IsFile(fp) {
		return fp, nil
	} else {
		return fp, ErrConfigNotFound
	}
	return "", ErrConfigNotFound
}
