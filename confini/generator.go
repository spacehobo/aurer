package confini

import (
	"bytes"
	"os"
	"reflect"
)

type ConfigGenerator struct {
	config     any
	filename   string
	serialized bytes.Buffer
}

func NewConfigGenerator(fn string, conf any) *ConfigGenerator {
	return &ConfigGenerator{
		config:   conf,
		filename: fn,
	}
}

func (ic *ConfigGenerator) Serialize() error {
	err := ConvToIni(&ic.serialized, ic.config)
	if err != nil {
		return err
	}
	return nil
}

func (i *ConfigGenerator) Marshal(s any) ([]byte, error) {
	var buf bytes.Buffer
	err := ConvToIni(&buf, s)
	if err != nil {
		panic("unexpected error of the conversion to INI")
	}
	return buf.Bytes(), nil
}

func (ic *ConfigGenerator) SaveToFile() error {
	fd, err := os.Create(ic.filename)
	if err != nil {
		return err
	}
	defer fd.Close()

	_, err = ic.serialized.WriteTo(fd)
	if err != nil {
		return err
	}
	return nil
}

func (ic *ConfigGenerator) LoadFromFile() error {
	fd, err := os.Open(ic.filename)
	if err != nil {
		return err
	}
	_, err = ic.serialized.ReadFrom(fd)
	if err != nil {
		return err
	}
	return nil
}

func ConvToIni(b *bytes.Buffer, s any) error {
	fields := reflect.VisibleFields(reflect.TypeOf(s))
	value := reflect.ValueOf(s)
	for _, f := range fields {
		key := ConvStructToFile(f.Name)
		val := ""
		switch v := value.FieldByName(f.Name); v.Kind() {
		case reflect.String:
			val = v.String()
			b.WriteString(key + " = " + val + "\n")
		case reflect.Bool:
			val = BoolConv{}.Output(v.Bool())
			b.WriteString(key + " = " + val + "\n")
		case reflect.Int64:
			val = Int64Conv{}.Output(v.Int())
			b.WriteString(key + " = " + val + "\n")
		case reflect.Struct:
			b.WriteString("\n[" + key + "]\n")
			ConvToIni(b, v.Interface())
		}
	}
	return nil
}

func CreateConfigFile(fname string, conf any) error {
	iniconf := NewConfigGenerator(fname, conf)

	err := iniconf.Serialize()
	if err != nil {
		return err
	}

	err = iniconf.SaveToFile()
	if err != nil {
		return err
	}

	return nil
}
