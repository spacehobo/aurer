package confini

import (
	"fmt"
	"reflect"
	"strings"
)

type ConfigParser struct {
	config    any
	filename  string
	fieldmap  FieldMapping
	conflines []string
	linenum   int
	section   any
}

type FieldMapping map[string]string

type step func() (step, error)

var stopExecution error = fmt.Errorf("stop execution")

var (
	debug   func(string) = func(s string) { /* fmt.Println("debug:",   s) */ }
	warning func(string) = func(s string) { /* fmt.Println("warning:", s) */ }
)

func NewConfigParser(cf any) *ConfigParser {
	config := &ConfigParser{}
	config.config = cf
	return config
}

func (cp *ConfigParser) Parse(fname string) error {
	cp.filename = fname

	config := reflect.Indirect(reflect.ValueOf(cp.config)).Interface()
	fm, err := MakeFieldMap(config)
	if err != nil {
		return err
	}
	cp.fieldmap = fm

	err = cp.Run()
	if err != nil {
		return err
	}

	return nil
}

func (cp *ConfigParser) LoadFile() error {
	lines, err := StringsFromFile(cp.filename)
	if err != nil {
		return err
	}
	cp.conflines = make([]string, 0)
	for _, l := range lines {
		cp.conflines = append(cp.conflines, strings.TrimSpace(l))
	}
	return nil
}

func (cp *ConfigParser) stepInit() (step, error) {
	debug("stepInit")
	cp.section = cp.config
	err := cp.LoadFile()
	if err != nil {
		return nil, err
	}
	cp.linenum = 0
	return cp.stepDetect, nil
}

func (cp *ConfigParser) stepNext() (step, error) {
	debug("stepNext")
	cp.linenum += 1
	return cp.stepDetect, nil
}

func (cp *ConfigParser) stepDetect() (step, error) {
	debug("stepDetect")
	if cp.linenum >= len(cp.conflines) {
		return cp.stepEnd, nil
	}
	row := cp.conflines[cp.linenum]
	if row == "" {
		return cp.stepNext, nil
	} else if strings.HasPrefix(row, ";") {
		return cp.stepNext, nil
	} else if strings.HasPrefix(row, "[") && strings.HasSuffix(row, "]") {
		return cp.stepSection, nil
	} else if strings.Contains(row, "=") {
		return cp.stepParameter, nil
	}

	return nil, fmt.Errorf("incorrect row #%d in configuration file: %s", cp.linenum, row)
}

func (cp *ConfigParser) stepSection() (step, error) {
	debug("stepSection")
	row := cp.conflines[cp.linenum]
	debug("processing section: " + strings.Trim(row, "[]"))
	key := strings.Trim(row, "[]")
	cp.section = GetStruct(cp.section, cp.fieldmap[key])
	return cp.stepNext, nil
}

func (cp *ConfigParser) stepParameter() (step, error) {
	debug("stepParameter")
	row := cp.conflines[cp.linenum]
	kv := strings.SplitN(row, "=", 2)
	key := strings.TrimSpace(kv[0])
	value := strings.TrimSpace(kv[1])
	debug("key: " + key)
	debug("value: " + value)

	err := SetStructValue(cp.section, cp.fieldmap[key], value)
	if err != nil {
		warning(err.Error())
	}
	return cp.stepNext, nil
}

func (cp *ConfigParser) stepEnd() (step, error) {
	debug("stepEnd")
	return nil, stopExecution
}

func (cp *ConfigParser) Run() error {
	debug("Run()")
	step, err := cp.stepInit()
	for err == nil {
		step, err = step()
	}
	if err != stopExecution {
		return err
	}
	return nil
}

func GetStruct(config any, field string) any {
	cfg := reflect.ValueOf(config).Elem()
	f := cfg.FieldByName(field)

	return f.Addr().Interface()
}

func SetStructValue(config any, field string, value string) error {
	cfg := reflect.ValueOf(config).Elem()
	f := cfg.FieldByName(field)

	switch f.Kind() {
	case reflect.String:
		f.SetString(value)
	case reflect.Int64:
		num, err := Int64Conv{}.Input(value)
		if err != nil {
			return err
		}
		f.SetInt(num)
	case reflect.Bool:
		cond, err := BoolConv{}.Input(value)
		if err != nil {
			return err
		}
		f.SetBool(cond)
	default:
		return fmt.Errorf("Unsupported type of field: %s", f.Kind().String())
	}
	return nil
}

func NewFieldMapping() *FieldMapping {
	fm := make(FieldMapping)
	return &fm
}

func MakeFieldMap(cf any) (FieldMapping, error) {
	fm := make(FieldMapping)

	structs := make([]reflect.Type, 0)
	structs = append(structs, reflect.TypeOf(cf))
	for _, fl := range reflect.VisibleFields(structs[0]) {
		if fl.Type.Kind() == reflect.Struct {
			structs = append(structs, fl.Type)
		}
	}

	for _, sl := range structs {
		fields := reflect.VisibleFields(sl)
		for _, fl := range fields {
			switch fl.Type.Kind() {
			case reflect.Struct:
				fm[ConvStructToFile(fl.Name)] = fl.Name
			case reflect.Bool, reflect.String:
				fm[ConvStructToFile(fl.Name)] = fl.Name
			case reflect.Int64:
				fm[ConvStructToFile(fl.Name)] = fl.Name
			default:
				return fm, fmt.Errorf(
					"Unsuppored type of the field: %s.%s\n", sl.Name(), fl.Name)
			}
		}
	}

	return fm, nil
}

func LoadConfig(fname string, cf any) error {
	err := NewConfigParser(cf).Parse(fname)
	if err != nil {
		return err
	}
	return nil
}
