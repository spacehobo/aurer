package confini

import (
	"bytes"
	"io"
	"os"
)

func IsFile(s string) bool {
	fi, err := os.Stat(s)
	if err != nil || fi.IsDir() {
		return false
	}
	return true
}

func IsDir(s string) bool {
	fi, err := os.Stat(s)
	if err != nil || !fi.IsDir() {
		return false
	}
	return true
}

func StringFromFile(fname string) (string, error) {
	var buf bytes.Buffer

	fd, err := os.Open(fname)
	if err != nil {
		return "", err
	}
	defer fd.Close()

	_, err = buf.ReadFrom(fd)
	if err != nil {
		return "", err
	}

	return buf.String(), nil
}

func StringsFromFile(fname string) ([]string, error) {
	lines := make([]string, 0)

	content, err := StringFromFile(fname)
	if err != nil {
		return lines, err
	}
	buf := bytes.NewBufferString(content)
	for {
		row, err := buf.ReadString(byte("\n"[0]))
		if err == io.EOF {
			break
		}
		lines = append(lines, row)
	}

	return lines, nil
}
