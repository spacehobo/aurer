package confini

import (
	"bytes"
	"fmt"
	"io"
	"reflect"
	"strings"
)

type ConfMetaData struct {
	Field string
	Param string
	Kind  reflect.Kind
}

type IniKV map[string]string

type SimpleIniParser struct {
	ini IniKV
}

func NewSimpleIniParser(f io.Reader) *SimpleIniParser {
	var b bytes.Buffer

	sp := &SimpleIniParser{}
	b.ReadFrom(f)
	sp.ini = ParseIni(b)
	return sp
}

func (si *SimpleIniParser) Parse(confref any) error {
	var confdata []ConfMetaData

	conf := reflect.Indirect(reflect.ValueOf(confref)).Interface()
	st := reflect.TypeOf(conf)
	if st.Kind() != reflect.Struct {
		return fmt.Errorf("Wrong parameter. Reference of struct required. Got a reference of %s", st.Kind().String())
	}

	for i := 0; i < st.NumField(); i++ {
		field := st.Field(i)
		if param, ok := field.Tag.Lookup("ini"); ok {
			cd := ConfMetaData{
				Field: field.Name,
				Param: param,
				Kind:  field.Type.Kind(),
			}
			confdata = append(confdata, cd)
		}
	}

	for _, cd := range confdata {
		if v, ok := si.ini[cd.Param]; ok {
			SetValue(confref, cd.Field, v)
		}
	}

	return nil
}

func SetValue(config any, field string, value string) error {
	cfg := reflect.Indirect(reflect.ValueOf(config))
	f := cfg.FieldByName(field)

	switch f.Kind() {
	case reflect.String:
		f.SetString(value)
	case reflect.Int64:
		num, err := Int64Conv{}.Input(value)
		if err != nil {
			return err
		}
		f.SetInt(num)
	case reflect.Bool:
		cond, err := BoolConv{}.Input(value)
		if err != nil {
			return err
		}
		f.SetBool(cond)
	default:
		return fmt.Errorf("Unsupported type of field: %s", f.Kind().String())
	}
	return nil
}

func ParseIni(b bytes.Buffer) IniKV {
	inikv := make(IniKV, 1)
	for {
		row, err := b.ReadString(byte("\n"[0]))
		if err == io.EOF {
			break
		}
		kv := strings.SplitN(row, "=", 2)
		inikv[strings.TrimSpace(kv[0])] = strings.TrimSpace(kv[1])
	}

	return inikv
}
