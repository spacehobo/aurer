package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

type LogConfig struct {
	file *os.File
}

func NewLogConfig(fn string) (*LogConfig, error) {
	logconf := LogConfig{}
	if fn == "" {
		log.SetOutput(io.Discard)
		return &logconf, nil
	}
	fd, err := os.OpenFile(fn, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return nil, fmt.Errorf("error opening file: %v", err)
	}
	logconf.file = fd

	log.SetOutput(logconf.file)
	log.SetPrefix("aurer: ")
	log.SetFlags(log.Flags() | log.Lmsgprefix)

	return &logconf, nil
}

func (lc *LogConfig) Close() {
	if lc.file != nil {
		lc.file.Close()
	}
}
