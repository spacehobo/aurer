module northbear/aurer

go 1.15

replace confini => ./confini

replace northbear/aurer/aurweb_rpc => ./aurweb_rpc

require (
	confini v0.0.0-00010101000000-000000000000 // indirect
	github.com/urfave/cli/v2 v2.27.2 // indirect
	northbear/aurer/aurweb_rpc v0.0.0-00010101000000-000000000000 // indirect
)
